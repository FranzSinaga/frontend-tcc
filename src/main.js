import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import VueApexCharts from 'vue-apexcharts'
import Notifications from 'vue-notification'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Tabs from 'vue-tabs-component'
    
Vue.use(VueAxios, axios)

Vue.config.productionTip = false
Vue.use(VueRouter)
Vue.component('apexchart', VueApexCharts)
Vue.use(Notifications)
Vue.use(Tabs);


// import Home from './components/beranda/Home.vue'
import BerandaUtama from './components/beranda/BerandaUtama.vue'
import BerandaAdmin from './components/beranda/BerandaAdmin.vue'
import BerandaPeserta from './components/beranda/BerandaPeserta.vue'
import BerandaInstruktur from './components/beranda/BerandaInstruktur.vue'
import Beranda from './components/beranda/Beranda.vue'
import ListKelas from './components/kelas/ListKelas.vue'
import TambahTopik from './components/topik/TambahTopik.vue'
import KelasBaru from './components/kelas/KelasBaru.vue'
import DetailKelasInstruktur from './components/kelas/DetailKelasInstruktur.vue'
import RegistrasiInstruktur from './components/registrasi/RegistrasiInstruktur.vue'


import ViewTopik from './components/beranda/ViewTopik.vue'
import KonfirmasiAkun from './components/beranda/KonfirmasiAkun.vue'
import DaftarPengajuan from './components/beranda/DaftarPengajuan.vue'
import DetailPengajuan from './components/beranda/DetailPengajuan.vue'
import Profile from './components/beranda/Profile.vue'
import EditProfile from './components/beranda/EditProfile.vue'

import Materi from './components/materi/Materi.vue'
import KategoriTopik from './components/topik/KategoriTopik.vue'
import DaftarPengajuanKelas from './components/kelas/DaftarPengajuanKelas.vue'
import DetailPengajuanKelas from './components/kelas/DetailPengajuanKelas.vue'
import DaftarPengajuanMateri from './components/materi/DaftarPengajuanMateri.vue'
import DetailPengajuanMateri from './components/materi/DetailPengajuanMateri.vue'
import store from './store'
import LaporanPeserta from './components/laporan/LaporanPeserta.vue'
import LaporanInstruktur from './components/laporan/LaporanInstruktur.vue'
import CreateMateri from './components/materi/CreateMateri.vue'
import MateriView from './components/materi/MateriView.vue'
import ExamDashboard from './components/exam/ExamDashboard'
import Register from './components/registrasi/Register.vue'



const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: [
    {path: '/', component: Beranda, name: "home"},
    {path: '/tcc', component: Beranda, name: "Beranda"},
    {path: '/BerandaUtama', component: BerandaUtama, name: "BerandaUtama"},
    {path: '/BerandaAdmin', component: BerandaAdmin, name: "BerandaAdmin"},
    {path: '/BerandaPeserta', component: BerandaPeserta, name: "BerandaPeserta"},
    {path: '/BerandaInstruktur', component: BerandaInstruktur, name: "BerandaInstruktur"},
    {path: '/ListKelas/:id', component: ListKelas, name: "ListKelas", props: true},
    {path: '/TambahTopik', component: TambahTopik, name: "TambahTopik"},
    {path: '/RegistrasiInstruktur', component: RegistrasiInstruktur, name: "RegistrasiInstruktur"},
    {path: '/KelasBaru', component: KelasBaru, name: "KelasBaru"},
    {path: '/Materi', component: Materi, name: "Materi"},    
    {path: '/DetailKelasInstruktur/:id', component: DetailKelasInstruktur, name: "DetailKelas", props: true},    
    {path: '/Materi', component: Materi, name: "Materi"},    
    {path: '/tcc/register', component: RegistrasiInstruktur, name: "RegistrasiInstruktur"},
    

    {path: '/ViewTopik', component: ViewTopik, name: "ViewTopik"},
    {path: '/KonfirmasiAkun', component: KonfirmasiAkun, name: "KonfirmasiAkun"},
    {path: '/DaftarPengajuan', component: DaftarPengajuan, name: "DaftarPengajuan"},
    {path: '/DetailPengajuan', component: DetailPengajuan, name: "DetailPengajuan"},
    {path: '/Profile', component: Profile, name: "Profile"},
    {path: '/EditProfile', component: EditProfile, name: "EditProfile"},

    {path: '/KategoriTopik', component: KategoriTopik, name: "KategoriTopik"},
    {path: '/DaftarPengajuanKelas', component: DaftarPengajuanKelas, name: "DaftarPengajuanKelas"},
    {path: '/DetailPengajuanKelas', component: DetailPengajuanKelas, name: "DetailPengajuanKelas"},
    {path: '/DaftarPengajuanMateri', component: DaftarPengajuanMateri, name: "DaftarPengajuanMateri"},
    {path: '/DetailPengajuanMateri', component: DetailPengajuanMateri, name: "DetailPengajuanMateri"},    
    {path: '/LaporanPeserta', component: LaporanPeserta, name: "LaporanPeserta"},
    {path: '/LaporanInstruktur/:id', component: LaporanInstruktur, name: "LaporanInstruktur", props: true},
    {path: '/create-materi/:courseName', component: CreateMateri, name: "CreateMateri", props:true},
    {path: '/materi-view', component: MateriView, name: "MateriView"},
    {path: '/exam', component: ExamDashboard, name: "ExamDashboard"},
    {path: '/register', component: Register, name: "Register"},

  ]
})

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app'),



new Vue({
  el: '#chart',
  components: {
    apexchart: VueApexCharts,
  },
  data: {
    series: [25, 15, 44, 55, 41, 17],
    chartOptions: {
      labels: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
      theme: {
        monochrome: {
          enabled: true
        }
      },
      title: {
        text: "Number of leads"
      },
      responsive: [{
        breakpoint: 480,
        options: {
          chart: {
            width: 200
          },
          legend: {
            position: 'bottom'
          }
        }
      }]
    }
  },

})

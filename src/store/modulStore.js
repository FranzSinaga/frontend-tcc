import Axios from "axios"

export default{
    state:{
        listModul:{
            
        },
    },

    getters:{
        getModul: state => {    
            return state.listModul
        },
    },

    mutations:{
        setModul:(state,payload) => {
            state.listModul = payload
        },
    },

    actions:{
        getAllModul({commit}, modul){
            Axios.get('https://demo5177791.mockable.io/tobacourse/modul/'+ modul)
            .then(Response => {
                // eslint-disable-next-line no-console
                console.log(Response.data.data)
                commit('setModul', Response.data.data)
            })
            .catch((e) => {
                // eslint-disable-next-line no-console
                console.log(e)
            })
        },         

        // editKelas()
    }
}
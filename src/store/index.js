import Vue from 'vue'
import Vuex from 'vuex'
import kelasStore from '@/store/kelasStore'
import topikStore from '@/store/topikStore'
import modulStore from '@/store/modulStore'

Vue.use(Vuex)

export default new Vuex.Store({
    modules:{
        kelasStore,
        topikStore,
        modulStore
    }
})

import Axios from "axios"

export default{
  state:{
    listTopik:{

    }
  },

  getters:{
    getTopik: state => {
      return state.listTopik
    }
  },

  mutations:{
    setTopik:(state,payload) => {
      state.listTopik = payload
    }
  }, 

  actions:{
    getAllTopik({commit}){      
      Axios.get('http://192.168.137.169:8000/api/category')
      // Axios.get('https://demo5177791.mockable.io/tobacourse/topik')
      .then(Response => {
        // console.log(Response.data)
        commit('setTopik', Response.data)        
      })
      .catch((e) => {
        // eslint-disable-next-line no-console
        console.log(e)
      })
    },        

    addTopik({commit}, topik) {                  
      Axios
      // .post('https://demo5177791.mockable.io/tobacourse/kelas', JSON.stringify(topik), {
      .post('http://192.168.137.169:8000/api/category/create', JSON.stringify(topik), {
        'headers': {'Content-Type': 'application/json'}
      })
      .then(response => {        
        // eslint-disable-next-line no-console
        console.log('success')                        
      }).catch((e) => {
        // eslint-disable-next-line no-console
        console.log(e)
      })
    },
    
    updateTopik({commit}, topik) {         
      console.log(topik)   
      // Axios.put('https://demo5177791.mockable.io/tobacourse/kelas/'+topik.id, JSON.stringify(topik), {
      Axios.put('http://192.168.137.169:8000/api/category/'+topik.id, JSON.stringify(topik), {
        'headers': {'Content-Type': 'application/json'}
      })
      .then(response => {        
        // eslint-disable-next-line no-console
        console.log('success')
        // console.log(kelas.name)      
        // console.log(topik.name_topik)
        // console.log(topik.desc_topik)
      })      
      .catch((e) => {              
        // eslint-disable-next-line no-console
        console.log(e)
      })
    }
  }
}
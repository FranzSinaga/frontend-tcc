import Axios from "axios"

export default{
    state:{
        listKelas:{
            
        },

        kelas:{

        }, 

        laporanKelas: {
            
        },
        laporanTahun: {
            
        }
    },

    getters:{
        getKelas: state => {    
            return state.listKelas
        },

        detailKelas: state => {
            return state.kelas
        },

        detailLaporan: state => {
            return state.laporanKelas
        },

        dataLaporanTahun: state => {
            return state.laporanTahun
        }
    },

    mutations:{
        setKelas:(state,payload) => {
            state.listKelas = payload
        },

        setDetail:(state,payload) => {
            state.kelas = payload
        },

        setLaporan:(state,payload) => {
            state.laporanKelas = payload
        },

        setLaporanTahun:(state,payload) => {
            state.laporanTahun = payload
        }
    },

    actions:{
        getAllKelas({commit}, kelas){
            // console.log(kelas)
            // Axios.get('http://192.168.43.214:8000/api/course/'+ kelas)
            Axios.get('https://demo5177791.mockable.io/tobacourse/kelas/'+ kelas)
            // console.log(kelas)
            .then(Response => {
                // eslint-disable-next-line no-console
                // console.log(Response.data)
                // commit('setKelas', Response.data)
                commit('setKelas', Response.data.data)
            })
            .catch((e) => {
                // eslint-disable-next-line no-console
                console.log(e)
            })
        }, 

        getDetailKelas({commit}, kelas){
            console.log(kelas)
            Axios.get('https://demo5177791.mockable.io/tobacourse/detailkelas/'+ kelas)
            .then(Response => {
                // console.log(Response.data.data)
                commit('setDetail', Response.data.data)                
            })
            .catch((e) => {
                // eslint-disable-next-line no-console
                console.log(e)
            })
        },   

        getLaporan({commit}, laporan){
            Axios.get('https://demo5177791.mockable.io/tobacourse/laporan/' + laporan)
            .then(Response => {
                // console.log(Response.data.data)  
                let out = []
                let lap = Response.data.data
                lap.forEach(laporan => {
                    out.push(laporan.periode)
                });
                // console.log(out)
                // commit('setDataGrafikTahun', out)
                commit('setLaporan', Response.data.data)
                // console.log(data.periode)
                // dispatch('setGrafikTahun', Response.data.data)
            })
            .catch((e) => {
                // eslint-disable-next-line no-console
                console.log(e)
            })
        },
        getLaporanTahun({commit}){
            Axios.get('https://demo5177791.mockable.io/tobacourse/laporan')
            .then(Response => {
                commit('setLaporanTahun', Response.data.data)
            })
            .catch((e) => {
                console.log(e)
            })
        },
        addKelas({commit}, kelas) {        
            console.log(kelas)
            let formData = new FormData();
            formData.append('course_name', kelas.course_name)
            formData.append('course_desc', kelas.course_desc)
            formData.append('price', kelas.price)
            formData.append('picture', kelas.picture)
            // formData.append('courseCategory', kelas.category_id)
            // console.log(formData)
            Axios
            .post('https://demo5177791.mockable.io/tobacourse/kelas', JSON.stringify(kelas), {
                // .post('http://192.168.43.214:8000/api/course/create/'+kelas.category_id, formData, {
                'headers': {'Content-Type': 'multipart/form-data'}
            })
            .then(response => {
                // eslint-disable-next-line no-console
                console.log('success')
                // console.log(kelas)
            }).catch((e) => {
                // eslint-disable-next-line no-console
                console.log("gagal")           
                console.log(e)
            })
        },

        // editKelas()
    }
}
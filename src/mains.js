import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import VueApexCharts from 'vue-apexcharts'
import Notifications from 'vue-notification'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Tabs from 'vue-tabs-component'
    
Vue.use(VueAxios, axios)

Vue.config.productionTip = false
Vue.use(VueRouter)
Vue.component('apexchart', VueApexCharts)
Vue.use(Notifications)
Vue.use(Tabs);


// import Home from './components/beranda/Home.vue'
import BerandaUtama from './components/beranda/BerandaUtama.vue'
import BerandaAdmin from './components/beranda/BerandaAdmin.vue'
import BerandaPeserta from './components/beranda/BerandaPeserta.vue'
import BerandaInstruktur from './components/beranda/BerandaInstruktur.vue'
import Beranda from './components/beranda/Beranda.vue'
import ListKelas from './components/kelas/ListKelas.vue'
import TambahTopik from './components/topik/TambahTopik.vue'
import KelasBaru from './components/kelas/KelasBaru.vue'
import DetailKelas from './components/kelas/DetailKelas.vue'
import RegistrasiInstruktur from './components/registrasi/RegistrasiInstruktur.vue'
import Materi from './components/materi/Materi.vue'
import KategoriTopik from './components/topik/KategoriTopik.vue'
import DaftarPengajuanKelas from './components/kelas/DaftarPengajuanKelas.vue'
import DetailPengajuanKelas from './components/kelas/DetailPengajuanKelas.vue'
import DaftarPengajuanMateri from './components/materi/DaftarPengajuanMateri.vue'
import DetailPengajuanMateri from './components/materi/DetailPengajuanMateri.vue'
import store from './store'
import LaporanPeserta from './components/laporan/LaporanPeserta.vue'
import LaporanInstruktur from './components/laporan/LaporanInstruktur.vue'
import CreateMateri from './components/materi/CreateMateri.vue'
import MateriView from './components/materi/MateriView.vue'
import ExamDashboard from './components/exam/ExamDashboard'


const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: [
    {path: '/', component: Beranda, name: "home"},
    {path: '/tcc', component: Beranda, name: "Beranda"},
    {path: '/BerandaUtama', component: BerandaUtama, name: "BerandaUtama"},
    {path: '/BerandaAdmin', component: BerandaAdmin, name: "BerandaAdmin"},
    {path: '/BerandaPeserta', component: BerandaPeserta, name: "BerandaPeserta"},
    {path: '/BerandaInstruktur', component: BerandaInstruktur, name: "BerandaInstruktur"},
    {path: '/ListKelas/:id', component: ListKelas, name: "ListKelas", props: true},
    {path: '/TambahTopik', component: TambahTopik, name: "TambahTopik"},
    // {path: '/register', component: RegistrasiInstruktur, name: "RegistrasiInstruktur"},
    {path: '/KelasBaru', component: KelasBaru, name: "KelasBaru"},
    {path: '/Materi', component: Materi, name: "Materi"},
    {path: '/DetailKelas', component: DetailKelas, name: "DetailKelas"},
    {path: '/tcc/register', component: RegistrasiInstruktur, name: "RegistrasiInstruktur"},
    {path: '/KategoriTopik', component: KategoriTopik, name: "KategoriTopik"},
    {path: '/DaftarPengajuanKelas', component: DaftarPengajuanKelas, name: "DaftarPengajuanKelas"},
    {path: '/DetailPengajuanKelas', component: DetailPengajuanKelas, name: "DetailPengajuanKelas"},
    {path: '/DaftarPengajuanMateri', component: DaftarPengajuanMateri, name: "DaftarPengajuanMateri"},
    {path: '/DetailPengajuanMateri', component: DetailPengajuanMateri, name: "DetailPengajuanMateri"},    
    {path: '/LaporanPeserta', component: LaporanPeserta, name: "LaporanPeserta"},
    {path: '/LaporanInstruktur', component: LaporanInstruktur, name: "LaporanInstruktur"},
    {path: '/create-materi/:courseName', component: CreateMateri, name: "CreateMateri", props:true},
    {path: '/materi-view', component: MateriView, name: "MateriView"},
    {path: '/exam', component: ExamDashboard, name: "ExamDashboard"},
  ]
})

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
